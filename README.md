This is our final TCD project for the PDM course in Computer Engineering Graduation at CAUPT-IFTM. 
Our focus is to reach teachers and help their teaching process by making them see some active methodology experiences. 
Active methodologies are teaching methods that insert students as the center of this process and, despite that, traditional teaching methods remain more popular.
Our mission is to spread this new methodologies, showing them with the app. As a consequence, teachers will use this learning/teaching tools by seeing how enriching they are.   

With love, Harpia team.
