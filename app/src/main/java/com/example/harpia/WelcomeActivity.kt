package com.example.harpia

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class WelcomeActivity : AppCompatActivity()
{

//    var firebaseUser: FirebaseUser? = null




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)

        val welcome_register_btn: Button = findViewById(R.id.welcome_register_btn)
        val welcome_login_btn: Button = findViewById(R.id.welcome_login_btn)

        welcome_register_btn.setOnClickListener {
            val intent = Intent(this@WelcomeActivity, RegisterActivity::class.java)
            startActivity(intent)
            finish()
        }

        welcome_login_btn.setOnClickListener {
            val intent = Intent(this@WelcomeActivity, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }
    }


    override fun onStart()
    {
        super.onStart()
//        firebaseUser = FirebaseAuth.getInstance().currentUser

//        if(firebaseUser != null)
//        {
//            val intent = Intent(this@WelcomeActivity, MainActivity::class.java)
//            startActivity(intent)
//            finish()
//        }
    }
}