package com.example.harpia

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import org.w3c.dom.Text

class RegisterActivity : AppCompatActivity()
{

//    private lateinit var mAuth: FirebaseAuth
//    private lateinit var refUsers: DatabaseReference
//    private var firebaseUserID: String = ""

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)


//        val toolbar: Toolbar = findViewById(R.id.toolbar_register)
//        setSupportActionBar(toolbar)
//        supportActionBar!!.title = "Register"
//        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
//        toolbar.setNavigationOnClickListener {
//            val intent = Intent(this@RegisterActivity, WelcomeActivity::class.java)
//            startActivity(intent)
//            finish()
//        }


//        mAuth = FirebaseAuth.getInstance()
        var register_btn: Button = findViewById(R.id.register_btn)
        register_btn.setOnClickListener {

            registerUser()

        }
    }

    private fun registerUser()
    {
        var register_btn: Button = findViewById(R.id.register_btn)
        var name_register: TextView = findViewById(R.id.name_register)
        var email_register: TextView = findViewById(R.id.email_register)
        var password_register: TextView = findViewById(R.id.password_register)
        var birthday_register: TextView = findViewById(R.id.birthday_register)
        var formation_register: TextView = findViewById(R.id.formation_register)
        var area_register: TextView = findViewById(R.id.area_register)
        var exp_register: TextView = findViewById(R.id.exp_register)


        val name: String = name_register.text.toString()
        val email: String = email_register.text.toString()
        val password: String = password_register.text.toString()
        val birthday: String = birthday_register.text.toString()
        val formation: String = formation_register.text.toString()
        val area: String = area_register.text.toString()
        val exper:String = exp_register.text.toString()

        if(name == "")
        {
            Toast.makeText(this@RegisterActivity,"Please Write Name.", Toast.LENGTH_LONG).show()
        }
        else if(email == "")
        {
            Toast.makeText(this@RegisterActivity,"Please Write email.", Toast.LENGTH_LONG).show()
        }
        else if(password == "")
        {
            Toast.makeText(this@RegisterActivity,"Please Write password.", Toast.LENGTH_LONG).show()
        }
        else if(birthday == "")
        {
            Toast.makeText(this@RegisterActivity,"Please Write birthday.", Toast.LENGTH_LONG).show()
        }
        else if(formation == "")
        {
            Toast.makeText(this@RegisterActivity,"Please Write formation.", Toast.LENGTH_LONG).show()
        }
        else if(area == "")
        {
            Toast.makeText(this@RegisterActivity,"Please Write area.", Toast.LENGTH_LONG).show()
        }
//        else
//        {
//            mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener { task ->
//                if(task.isSuccessful)
//                {
//                    firebaseUserID = mAuth.currentUser!!.uid
//                    refUsers = FirebaseDatabase.getInstance().reference.child("Users").child(firebaseUserID)
//
//                    val userHashMap = HashMap<String, Any>()
//                    userHashMap["uid"] = firebaseUserID
//                    userHashMap["name"] = name
//                    userHashMap["profile"] = "https://firebasestorage.googleapis.com/v0/b/harpia-5d62d.appspot.com/o/profile.jpg?alt=media&token=e30d3f90-5504-4bd7-94a8-aba9ae1a8d20"
//                    userHashMap["cover"] = "https://firebasestorage.googleapis.com/v0/b/harpia-5d62d.appspot.com/o/cover.jpg?alt=media&token=66a031ac-0e56-42f5-88d2-3d882033f6cb"
//                    userHashMap["status"] = "offline"
//                    userHashMap["search"] = name.toLowerCase()
//                    userHashMap["facebook"] = "https://m.facebook.com"
//                    userHashMap["instagram"] = "https://m.instagram.com"
//                    userHashMap["website"] = "https://www.google.com"
//
//                    refUsers.updateChildren(userHashMap).addOnCompleteListener { task ->
//                        if(task.isSuccessful)
//                        {
//                            val intent = Intent(this@RegisterActivity, MainActivity::class.java)
//                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
//                            startActivity(intent)
//                            finish()
//                        }
//                    }
//                }
//                else
//                {
//                    Toast.makeText(this@RegisterActivity,"Error Message: " + task.exception!!.message.toString(), Toast.LENGTH_LONG).show()
//                }
//            }
//        }
    }
}